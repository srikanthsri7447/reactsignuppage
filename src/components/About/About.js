import React from 'react'
import './About.css'

function About() {
    return (
        <div className='container'>
            <div className='row'>
                <div className='container col-12 cus-lh'>
                    <div className='row cardContainer d-flex align-items-center bg-white mt-3 lh-1'>
                    <div className='col-6'>
                        <img src='https://martialvolvocars.in/wp-content/uploads/2016/04/extfeat2RDesignPolestarS6005.jpg' alt='about-img'/>
                    </div>

                    <div className='col-6'>
                        <h1>Made by Sweden</h1>
                        <p>In Sweden, care for people is a priority. Everyone is important – all life is important – and that has always carried through naturally into Volvo’s ethos of car making. It’s this care for the wellbeing of driver, passengers and everyone around the car that has always defined Volvo. And always will.</p>
                    </div>
                </div>

                </div>
                <div className='container col-12 cus-lh'>
                    <div className='row cardContainer d-flex align-items-center bg-white mt-3 lh-1'>
                    <div className='col-6'>
                        <h1>One million lives</h1>
                        <p>Keeping you safe has always been Volvo’s mission. And the humble safety belt has helped us save an estimated one million lives since Volvo’s Nils Bohlin introduced the three-point safety belt back in 1959. Since then, we’ve been on a constant path of innovation – right up to creating the world’s first seat belts that pre-tighten when the car senses that a rear impact is imminent.</p>
                    </div>
                    <div className='col-6'>
                        <img src='https://martialvolvocars.in/wp-content/uploads/2016/04/One_million_lives_extfeat2.jpg' alt='about-img'/>
                    </div>
                </div>

                </div>
            </div>

        </div>
    )
}

export default About