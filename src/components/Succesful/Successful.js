import React from 'react'
import './Successful.css'

function Successful(props) {

    const {firstName,lastName,role,email} = props.details

  return (
    <div className='succesContainer'>
        <h4>Hello {firstName} {lastName}</h4>
        <p>Your account has been created successfully</p>
        <p>We are happy to have you onboard as {role}</p>
        <p>We will send further details to your registered email: {email}</p>
    </div>
  )
}

export default Successful