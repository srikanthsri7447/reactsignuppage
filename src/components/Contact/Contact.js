import React from 'react'

function Contact() {
  return (
    <div className="mt-2 d-flex flex-column align-items-center bg-white">
      <div className='w-25'>
    <img
      src="https://assets.ccbp.in/frontend/react-js/contact-blog-img.png"
      alt="contact"
    />
      </div>
    <h1 className="contact-heading">Contact</h1>
  </div>
  )
}

export default Contact