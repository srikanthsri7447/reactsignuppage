import React from 'react'

function Home() {
  return (
    <div className='homeContainer d-flex flex-column align-items-center text-white mt-2'>
        <h1 className='fw-bold'>XC60</h1>
        <p>A safe way to move a family around in style.</p>
    </div>
  )
}

export default Home