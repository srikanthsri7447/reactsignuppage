import Header from './components/Header/Header';
import Signup from './components/Signup/Signup';
import Home from './components/Home/Home';
import About from './components/About/About';
import Contact from './components/Contact/Contact';
import Successful from './components/Succesful/Successful';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';

function App() {
  
  return (
    <div className='App'>
      <BrowserRouter >

        <Header />

        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path='/about' element={<About />} />
          <Route exact path='/contact' element={<Contact />} />
          <Route exact path='/Successful' element={<Successful  />} />
          <Route exact path='/signup' element={<Signup />} />
        </Routes>

      </BrowserRouter>
    </div>

  );
}

export default App;
