import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

function Header() {
    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light bg-light ">
                <div className="container-fluid">
                    <h2 className='welcomeText'>VOLVO</h2>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav flex-row justify-content-center">
                            <li className="nav-item me-3">
                                <Link to='/'>Home</Link>
                            </li>
                            <li className="nav-item me-3">
                                <Link to='/about'>About</Link>
                            </li>
                            <li className="nav-item me-3">
                                <Link to='/contact'>Contact</Link>
                            </li>
                            <li className="nav-item me-3">
                                <Link to='/signup'>Signup</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

    )
}

export default Header